class AddStatusOnArea < ActiveRecord::Migration[5.1]
  def change
    add_column :areas, :status, :boolean
    add_column :subareas, :status, :boolean
  end
end
