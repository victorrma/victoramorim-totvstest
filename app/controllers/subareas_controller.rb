class SubareasController < ApplicationController
  before_action :set_subarea, only: [:edit, :update, :show, :destroy]

  def new
    @subarea = Subarea.new
  end

  def create
    @subarea = Subarea.new(subarea_params)
    if @subarea.save
      flash[:success] = "Subarea foi criada!"
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @subarea.update(area_params)
      flash[:success] = "Subárea atualizada com sucesso!"
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def show
  end

  def destroy
    @subarea.destroy
    flash[:danger] = "SubÁrea Deletada!"
    redirect_to root_path
  end

  private

  def subarea_params
    params.require(:subarea).permit(:name, :status, :area_id)
  end
  def set_subarea
    @subarea = Subarea.find(params[:id])
  end
end
