class AreasController < ApplicationController
  before_action :set_area, only: [:edit, :update, :show, :destroy]


  def new
    @area = Area.new
  end

  def create
    @area = Area.new(area_params)
    if @area.save
      flash[:success] = "Area foi criada!"
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @area.update(area_params)
      flash[:success] = "Área atualizada com sucesso!"
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def show
  end

  def destroy
    @area.destroy
    flash[:danger] = "Área Deletada!"
    redirect_to root_path
  end

  private

  def area_params
    params.require(:area).permit(:name, :status)
  end
  def set_area
    @area = Area.find(params[:id])
  end
end
