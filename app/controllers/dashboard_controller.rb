class DashboardController < ApplicationController

  def index
    @areas = Area.all
    @filter = Filter.new
  end

  def filter
    @filter = Filter.new(params[:filter])

    if @filter.type.eql? "SubArea"
      @result = Subarea.find_by(name: @filter.query, status: @filter.status)
    else
      @result = Area.find_by(name: @filter.query, status: @filter.status)
    end
  end
end
