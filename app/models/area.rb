class Area < ActiveRecord::Base
  has_many :subareas

  validates :name, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { maximum: 100 }

end
