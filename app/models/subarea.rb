class Subarea < ActiveRecord::Base
  belongs_to :area

  validates :name, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { maximum: 100 }
end
