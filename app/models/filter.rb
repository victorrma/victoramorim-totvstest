class Filter
  include ActiveModel::Model

  attr_accessor :query, :type, :status

  def initialize(attributes={})
    @type = attributes[:type]
    @query = attributes[:query]

    if attributes[:status] == "1"
      @status = true
    else
      @status = false
    end
  end

end
