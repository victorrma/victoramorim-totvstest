Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'dashboard#index'
  post 'filter', to: 'dashboard#filter'


  resources :areas
  resources :subareas
end
